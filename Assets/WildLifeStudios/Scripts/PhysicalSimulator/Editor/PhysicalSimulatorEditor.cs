using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace WildLifeStudios.PhysicalSimulator
{
    [ExecuteInEditMode]
    public class PhysicalSimulatorEditor : EditorWindow
    {
        public List<GameObject> gameObjectsList;
        public List<Component> addedComponents;

        private bool isPhysicsOn;

        [MenuItem("Automation/Physical Simulator")]
        public static void ShowWindow()
        {
            EditorWindow cubeWindow = EditorWindow.GetWindow<PhysicalSimulatorEditor>("Physical Simulatorb");
        }
        private void Update()
        {
            if (isPhysicsOn) {
                Physics.autoSimulation = !isPhysicsOn;
                Physics.Simulate(Time.fixedDeltaTime);
                Physics.autoSimulation = isPhysicsOn;
            }
        }
        void OnGUI()
        {
            SerializedObject serialObj = new SerializedObject(this);
            SerializedProperty serialProp = serialObj.FindProperty("gameObjectsList");

            EditorGUILayout.PropertyField(serialProp, true);
            serialObj.ApplyModifiedProperties();

            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Run")) {
                SetPhysicsState(true);
            }
            if (GUILayout.Button("Stop")) {
                SetPhysicsState(false);
            }
            if (GUILayout.Button("Reset List")) {
                gameObjectsList = new List<GameObject>();
            }
            GUILayout.EndHorizontal();
            GUILayout.Label("Physics state: " + (isPhysicsOn ? "ON" : "OFF"));
        }
        private void SetPhysicsState(bool run)
        {
            GameObject go;
            if (run) {
                addedComponents = new List<Component>();
                for (int i = 0; i < gameObjectsList.Count; i++) {
                    go = gameObjectsList[i];
                    AddComponent(go, typeof(MeshCollider));
                    AddComponent(go, typeof(Rigidbody));
                }
            } else {
                for (int i = 0; i < addedComponents.Count; i++) {
                    GameObject.DestroyImmediate(addedComponents[i]);
                }
                EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
            }
            isPhysicsOn = run;
        }

        private void AddComponent(GameObject go, Type type)
        {
            Component addedComponent;
            if (go.GetComponent(type) == null) {
                go.AddComponent(type);
                addedComponent = go.GetComponent(type);
                addedComponents.Add(addedComponent);
                if (addedComponent is MeshCollider)
                    ((MeshCollider)addedComponent).convex = true;
            }
        }
    }
}
