﻿using System;
using UnityEditor;
using UnityEngine;

namespace WildLifeStudios.ScriptCleaner
{
    [InitializeOnLoad]
    public class ScriptCleaner
    {
        static GameObject rightClickedGameobject;
        private static int anyEventHash, correctEventHash;

        [MenuItem("GameObject/Automation/Remove Scripts", false, 0)]
        public static void RemoveScripts()
        {
            // roque: validate the current RMB click event to prevent an older right clicked gameobject to be the target
            if (anyEventHash != correctEventHash) rightClickedGameobject = null;

            try {
                // roque: get all components from the object and it's children, but only destroy others than the Transform
                Component[] allComponentsInChildren = rightClickedGameobject.GetComponentsInChildren(typeof(Component));
                foreach (var item in allComponentsInChildren) {
                    if (item is Transform)
                        continue;
                    GameObject.DestroyImmediate(item);
                }

                // roque: success message
                EditorUtility.DisplayDialog("Scrips removed!", "All scripts have been removed from the gameobject ans it's children.", "Ok", "");

                // roque: makes the scene dirty to easier reproduce the script as many times as you wish by discarding scene changes
                EditorUtility.SetDirty(rightClickedGameobject);
            }
            catch (Exception ex) {
                // roque: fallback message in case of anything goes wrong
                EditorUtility.DisplayDialog("Error while removing scrips!", ex.Message, "Ok", "");
            }

        }

        static ScriptCleaner()
        {
            EditorApplication.hierarchyWindowItemOnGUI += CheckRMB;
        }

        private static void CheckRMB(int instanceID, Rect selectionRect)
        {
            if (Event.current != null
                && Event.current.button == 1
                && Event.current.type == EventType.MouseDown) {

                // roque: refreshes this field with any RMB event...
                anyEventHash = Event.current.GetHashCode();

                GameObject clickedObject = EditorUtility.InstanceIDToObject(instanceID) as GameObject;
                if (clickedObject && selectionRect.Contains(Event.current.mousePosition)) {

                    // roque: ...but only stores in this other field the hash os a successfully found gameobject under the mouse pointer
                    correctEventHash = Event.current.GetHashCode();
                    rightClickedGameobject = clickedObject;
                }
            }
        }
    }
}
