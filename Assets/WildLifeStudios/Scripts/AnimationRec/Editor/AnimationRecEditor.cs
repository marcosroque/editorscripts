using UnityEngine;
using UnityEditor;
using System.IO;

namespace WildLifeStudios.AnimationRec
{
    [CustomEditor(typeof(AnimationRec))]
    public class AnimationRecEditor : Editor
    {
        private static AnimationRec myTarget;

        public override void OnInspectorGUI()
        {
            if (!myTarget)
                myTarget = (AnimationRec)target;

            myTarget.CheckFields();
            DrawDefaultInspector();

            if (GUILayout.Button("Browse Save Folder")) {
                myTarget.recordFolder = EditorUtility.SaveFolderPanel("", "", myTarget.recordFolder);
            }
        }

        [InitializeOnLoadMethod]
        static void EditorInit()
        {
            System.Reflection.FieldInfo info = typeof(EditorApplication).GetField("globalEventHandler", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic);
            EditorApplication.CallbackFunction value = (EditorApplication.CallbackFunction)info.GetValue(null);
            value += EditorGlobalKeyPress;
            info.SetValue(null, value);
        }

        static void EditorGlobalKeyPress()
        {
            if (!myTarget) return;
            Event e = Event.current;
            switch (e.type) {
                case EventType.KeyDown:
                    if (e.keyCode == myTarget.startRecordKey) {
                        bool success = myTarget.StartRecording();
                        EditorUtility.DisplayDialog("Recording STARTED", success ? myTarget.clipName : "ERROR", "Ok", "");
                    }
                    if (e.keyCode == myTarget.finishRecordKey) {
                        bool success = myTarget.FinishRecording();
                        if (success) {
                            RecordAnimationAsset();
                        }
                        EditorUtility.SetDirty(myTarget.gameObject);
                        EditorUtility.DisplayDialog("Recording FINISHED", success ? myTarget.clipName : "ERROR", "Ok", "");
                    }
                    if (e.keyCode == myTarget.deleteRecordKey) {
                        string filePath = Path.Combine(myTarget.recordFolder, myTarget.clipName + ".anim");
                        if (!string.IsNullOrEmpty(filePath) && File.Exists(filePath)) {
                            AssetDatabase.DeleteAsset(filePath);
                        }
                        EditorUtility.DisplayDialog("Recording DELETED", string.IsNullOrEmpty(filePath) ? "ERROR" : filePath, "Ok", "");
                    }
                    break;
            }
        }

        private static void RecordAnimationAsset()
        {
            // Checks for existing folder
            if (!Directory.Exists(myTarget.recordFolder)) {
                Directory.CreateDirectory(myTarget.recordFolder);
            }

            // Serialize file name creation
            int nameIndex = 0;
            string formatString = "{0}.anim";
            string newFileName = string.Format(formatString, myTarget.clipName);
            string filePath = Path.Combine(myTarget.recordFolder, newFileName);

            // Increments index if file already exists
            formatString = "{0}-{1}.anim";
            while (File.Exists(filePath)) {
                newFileName = string.Format(formatString, myTarget.clipName, nameIndex.ToString("000"));
                filePath = Path.Combine(myTarget.recordFolder, newFileName);
                nameIndex++;
            }

            // Save asset to disk
            AssetDatabase.CreateAsset(myTarget.currentAnimationClip, filePath);
            AssetDatabase.SaveAssets();
        }
    }
}
