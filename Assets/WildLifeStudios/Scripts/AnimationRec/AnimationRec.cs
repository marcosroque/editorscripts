using System.Collections;
using UnityEditor.Animations;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace WildLifeStudios.AnimationRec
{
    [ExecuteInEditMode]
    public class AnimationRec : MonoBehaviour
    {
        public string recordFolder;
        public string clipName = "";
        public KeyCode startRecordKey = KeyCode.I, finishRecordKey = KeyCode.O, deleteRecordKey = KeyCode.P;
        public float recordingFrequency = 0.25f;

        public AnimationClip currentAnimationClip;

        private Coroutine recRoutine;
        private GameObjectRecorder gameObjectRecorder;

        public void CheckFields()
        {
            if (string.IsNullOrEmpty(recordFolder)) {
                Scene activeScene = SceneManager.GetActiveScene();
                recordFolder = activeScene.path.Substring(0, activeScene.path.LastIndexOf("/"));
            }
            if (string.IsNullOrEmpty(clipName)) clipName = transform.name + "_Animation";
        }

        public bool StartRecording()
        {
            if (recRoutine != null)
                return false;

            currentAnimationClip = new AnimationClip();
            currentAnimationClip.legacy = true;
            currentAnimationClip.name = clipName;
            
            gameObjectRecorder = new GameObjectRecorder(gameObject);
            gameObjectRecorder.ResetRecording();
            gameObjectRecorder.BindComponentsOfType<Transform>(gameObject, true);

            recRoutine = StartCoroutine(RecordRoutine());
            return true;
        }
        IEnumerator RecordRoutine()
        {
            while (true) {
                gameObjectRecorder.TakeSnapshot(recordingFrequency);
                yield return new WaitForSeconds(recordingFrequency);
            }
        }

        public bool FinishRecording()
        {
            if (recRoutine == null)
                return false;

            StopCoroutine(recRoutine);
            recRoutine = null;
            if (gameObjectRecorder.isRecording)
                gameObjectRecorder.SaveToClip(currentAnimationClip);

            Animation animationsHolder = GetComponent<Animation>();
            if (animationsHolder != null)
                animationsHolder.AddClip(currentAnimationClip, clipName);
            return true;
        }

    }
}
